//
//  ViewController.swift
//  APIDemo
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
//import Particle_SDK
import WatchConnectivity


class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
        let name = message["name"] as! [String : Any]
        print("HELLO WORLD:\(name)")
        cityName.text = ("\(name)")
        
            
    }

    

    
    
    
    
    // MARK: User variables
    let USERNAME = "prudhvi.satram1995@gmail.com"
    let PASSWORD = "Prudhvi@2914"
    // MARK: Outlets
    let DEVICE_ID = "22003f001247363333343437"
    //  var myPhoton : ParticleDevice?
    
    
    var currentTime:String?

     var count = 0

    @IBOutlet weak var cityName: UILabel!
    
    
    @IBOutlet weak var timelabel: UILabel!
    
    @IBOutlet weak var templabel: UILabel!
    
    
    @IBOutlet weak var weatherdeslabel: UILabel!
    
    // MARK: Variables
    var cityCoordinates:CLLocationCoordinate2D?
    //---------------------------
    
    
    
           override func viewDidLoad() {
           super.viewDidLoad()
            //calls supdateTimeOnParticle every 5 secs
            let timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { timer in
                print("Timer fired!")
                self.count += 1
             
                
//                func updateTimeOnParticle() {
//
//                       let parameters = [String(self.currentTime)]
//
//                       var task = myPhoton!.callFunction("score", withArguments: parameters) {
//                           (resultCode : NSNumber?, error : Error?) -> Void in
//                           if (error == nil) {
//                            //test
//                               print("SenDMessage: \(self.currentTime)")
//                           }
//                           else {
//                               print("ERROR")
//                           }
//                       }
//
//
//                   }
                
            }
            if (WCSession.isSupported() == true) {
                       
                      print("WC is supported!")
                       
                       // create a communication session with the phone
                       let session = WCSession.default
                       session.delegate = self
                       session.activate()
                   }
                   else {
                       print("WC NOT supported!")
                   }
        // Do any additional setup after loading the view, typically from a nib.
           let URL = "http://api.weatherstack.com/current?access_key=39e25ed31b7ece5548901e56e75a25f8&query=Chicago&units=m"
        
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            
            // OUTPUT the entire json response to the terminal
            print(apiData)
            
            
            // GET sunrise/sunset time out of the JSON response
            let jsonResponse = JSON(apiData)
//            let sunriseTime = jsonResponse["results"]["sunrise"].string
//            let sunsetTime = jsonResponse["results"]["sunset"].string
            
            let cityName = jsonResponse["location"]["name"].stringValue

                        let temperature = jsonResponse["current"]["temperature"].stringValue
                        let currentTime = jsonResponse["location"]["localtime"].stringValue
                        let weatherdescription = jsonResponse["current"]["weather_descriptions"][0].stringValue
                        
                        print(temperature)
                        print(currentTime)
                        print(weatherdescription)
                        print(cityName)
            
            
            // display in a UI
            self.templabel.text = ("\(temperature)")
                          self.timelabel.text = ("\(currentTime)")
                          self.weatherdeslabel.text = ("\(weatherdescription)")
                          self.cityName.text = ("\(cityName)")
            // DEBUG:  Output it to the terminal
//            print("Sunrise: \(sunriseTime)")
//            print("Sunset: \(sunsetTime)")
            
            // display in a UI
//            self.sunriseLabel.text = "\(sunriseTime!)"
//            self.sunsetLabel.text = "\(sunsetTime!)"
            
            
            
            
        }
        
        
    
            self.geocode(cityName: "Toronto")
            
//
//            let preferences = UserDefaults.standard
//
//            print("SHARED PREFERENCES OUTPUT")
//            print(preferences.string(forKey: "time"))
//            print(preferences.string(forKey: "temp"))
//            print(preferences.string(forKey: "city"))
            
            
            
               // 1. Initialize the SDK
           //    ParticleCloud.init()
               
               // 2. Login to your account
//               ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
//                   if (error != nil) {
//                       // Something went wrong!
//                       print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
//                       // Print out more detailed information
//                       print(error?.localizedDescription)
//                   }
//                   else {
//                       print("Login success!")
//                       self.getDeviceFromCloud()
//                   }
//               }//end login
    }
  
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    func getDeviceFromCloud() {
//    ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
//
//            if (error != nil) {
//                print("Could not get device")
//                print(error?.localizedDescription)
//                return
//            }
//            else {
//                print("Got photon: \(device?.id)")
//                self.myPhoton = device
//            }
//
//        } // end getDevice()
//    }
//


    func geocode(cityName:String) {
        let geocoder = CLGeocoder()
        print("Trying to geocode \(cityName) to lat/lng")
        geocoder.geocodeAddressString(cityName) {
            
            (placemarks, error) in
            
            print("Got a response")
            // Process Response
            if let error = error {
                print("Unable to Forward Geocode Address (\(error))")
               // self.savedCityLabel.text = "Unable to Find Location for Address"
                self.cityCoordinates = nil
            } else {
                var location: CLLocation?
                
                if let placemarks = placemarks, placemarks.count > 0 {
                    location = placemarks.first?.location
                    self.cityCoordinates = location?.coordinate
                }
                
                if let location = location {
                    self.cityCoordinates = location.coordinate
                    
                } else {
                    print("No matching location found")
                 //   self.savedCityLabel.text = "No Matching Location Found"
                    self.cityCoordinates = nil
                }
            }
            
            print("City coordinates: \(self.cityCoordinates)")
        }
    }
    
    
    
    
}

