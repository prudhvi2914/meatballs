// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton button = InternetButton();
void setup() {
  button.begin();

  // Exposed functions-test

  // 1. showing the score
  // http://api.particle.io/<deviceid>/score
  Particle.function("score", displayScore);


  // Show a visual indication to the player that the Particle
  // is loaded & ready to accept inputs
  for (int i = 0; i < 3; i++) {
    button.allLedsOn(20,0,0);
    delay(250);
    button.allLedsOff();
    delay(250);
  }
}



int DELAY = 200;

void loop() {

 
  
}

int displayScore(String cmd) {
  // reset the displayed score
  button.allLedsOff();

  // turn on the specified number of lights
  int score = cmd.toInt();

  if (score < 0 || score > 11) {
    // error: becaues there are only 11 lights
    // return -1 means an error occurred
    return -1;
  }

  for (int i = 1; i <= score; i++) {
      button.ledOn(i, 255, 255, 0);
  }

  // return = 1 means the function finished running successfully
  return 1;
}



