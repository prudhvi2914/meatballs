//
//  CitySettingsInterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Prdhvi on 2019-03-04.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

class CitySettingsInterfaceController: WKInterfaceController {

    // MARK: Outlets
    @IBOutlet var savedCityLabel: WKInterfaceLabel!
    @IBOutlet var loadingImage: WKInterfaceImage!
    @IBOutlet var saveButtonLabel: WKInterfaceLabel!

    // MARK: Variables
    var city:String!
    
    var temp:String?
    var time:String?

    // MARK: API KEYS
    let API_KEY = "39e25ed31b7ece5548901e56e75a25f8"
    var mainVC:InterfaceController?
    
    @IBAction func selectCityPressed() {
        // 1. When person clicks on button, show them the input UI
        let suggestedResponses = ["Toronto", "Montreal","Hyderabad","Chicago"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.savedCityLabel.setText(userResponse)
                self.city = userResponse
            }
        }
    }
    
    @IBAction func saveButtonPressed() {
        print("Getting City")
        self.geocode(cityName: self.city)
        
        
    }
 
    
    func geocode(cityName:String) {
        
        // Get lat long using LocationIQ API (https://locationiq.com)
        // You can use Google, but you need to give Google your credit card number.
        // You can use the built in Apple CoreLocation library, but this library doesn't work properly with the emulator. See notes below.
        
        
        // TODO: Put your API call here
        
        // Encode the city name
        let cityParam = cityName.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        
        // start showing the loading image
        
        self.loadingImage.setImageNamed("Progress")
        self.loadingImage.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 1, repeatCount: 0)
        self.saveButtonLabel.setText("Saving...")
        
        
            let URL = "http://api.weatherstack.com/current?access_key=\(self.API_KEY)&query=\(cityParam!)&units=m"
            print(URL)
            Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                
                return
            }
            
            print("Printing apidata\(apiData)")
            
            // Get the lat/long component out of the response url
            let jsonResponse = JSON(apiData)
            //printing the data
//
//            print("TRYING:To Print:::::\(jsonResponse)")
                
            
            
            
//            let results = jsonResponse.array?.first
                let results = jsonResponse

            print("HElloWorld:::\(results)")
            
            
            
            if (results == nil) {
                print("Error parsing results from JSON response")
                return
            }
            
            let data = JSON(results)
            print(data)
            
            
            self.time = data["location"]["localtime"].string
            self.temp = data["current"]["temperature"].string
                self.city = data["location"]["name"].string

//          print("Lat: \(self.latitude)")
//          print("Long: \(self.longitude)")
                print("CurrentTimeKKKK:\(self.time)")
                print("CurrentTempINKKK:\(self.temp)")
            
            // save this to Shared Preferences
            let preferences = UserDefaults.standard
            preferences.set(self.time, forKey:"time")
            preferences.set(self.temp, forKey:"temp")
            preferences.set(self.city, forKey:"city")
            
            // dismiss the View Controller
            self.popToRootController()
            
            
            self.loadingImage.stopAnimating()
        }
        
        
    }
    
    // MARK: Default functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        // get name of city in shared preferences
        let preferences = UserDefaults.standard
        guard let savedCity = preferences.string(forKey: "savedCity") else {
            return
            
             
            
            
        }
        
        self.savedCityLabel.setText(savedCity)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
