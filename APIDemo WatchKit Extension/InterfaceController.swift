//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Prudhvi on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
import WatchConnectivity

class InterfaceController: WKInterfaceController,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

//    // MARK: Outlets
//    @IBOutlet var sunriseLabel: WKInterfaceLabel!
//    @IBOutlet var sunsetLabel: WKInterfaceLabel!
//    @IBOutlet var cityLabel: WKInterfaceLabel!
    //---------------------------
    
    @IBOutlet var cityName: WKInterfaceLabel!

    @IBOutlet var temparature: WKInterfaceLabel!
    
    
    @IBOutlet var Time: WKInterfaceLabel!
    @IBOutlet var weatherdesc: WKInterfaceLabel!
    
    
    @IBOutlet var loadingSunriseImage: WKInterfaceImage!
    @IBOutlet var loadingSunsetImage: WKInterfaceImage!
    //39e25ed31b7ece5548901e56e75a25f8

    
    // MARK: variables
    var cityCoordinates:CLLocationCoordinate2D?
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported() == true) {
                   
                  print("WC is supported!")
                   
                   // create a communication session with the phone
                   let session = WCSession.default
                   session.delegate = self
                   session.activate()
               }
               else {
                   print("WC NOT supported!")
               }
        

             
        
        let preferences = UserDefaults.standard
        
        print("SHARED PREFERENCES OUTPUT")
        print(preferences.string(forKey: "time"))
        print(preferences.string(forKey: "temp"))
        print(preferences.string(forKey: "city"))
//
//
//        var lat = preferences.string(forKey:"savedLat")
//        var lng = preferences.string(forKey:"savedLng")
        var city = preferences.string(forKey:"city")
        
        if (city == nil) {
            
            city = "Toronto"
           
        }
        print("SENDING MESGGAGE TO PHONE")
                      let message = ["name":"hi"] as! [String : Any]
                       WCSession.default.sendMessage(message, replyHandler: nil)
        
        // Update UI
       //  self.cityName.setText(city)
        
        // start animations
//        self.showLoadingAnimations()
        
        // TODO: Put your API call here
        
//        let URL = "https://api.sunrise-sunset.org/json?lat=\(lat!)&lng=\(lng!)&date=today"
        
         let URL = "http://api.weatherstack.com/current?access_key=39e25ed31b7ece5548901e56e75a25f8&query=\(city!)&units=m"
        
              print("Url: \(URL)")
             Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            
        //    print(apiData)
            // GET sunrise/sunset time out of the JSON response
                let jsonResponse = JSON(apiData)
                
                let cityName = jsonResponse["location"]["name"].stringValue

                let temperature = jsonResponse["current"]["temperature"].stringValue
                let currentTime = jsonResponse["location"]["localtime"].stringValue
                let weatherdescription = jsonResponse["current"]["weather_descriptions"][0].stringValue
                
                print(temperature)
                print(currentTime)
                print(weatherdescription)
                print(cityName)
                
                

            // display in a UI
                self.temparature.setText("\(temperature)")
                self.Time.setText("\(currentTime)")
                self.weatherdesc.setText("\(weatherdescription)")
                self.cityName.setText("\(cityName)")
                

            // stop the loading animation
                self.stopAnimations()
        }
        
        
    }
    
        func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
            
         
            
 
        
        }

   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    @IBAction func changeCityButtonPressed() {

        
        
    }
    
//    func showLoadingAnimations() {
//        // animate the sunrise label
//        self.loadingSunriseImage.setImageNamed("Progress")
//        self.loadingSunriseImage.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 1, repeatCount: 0)
//        self.sunriseLabel.setText("Updating...")
//
//        // animate the sunset label
//        self.loadingSunsetImage.setImageNamed("Progress")
//        self.loadingSunsetImage.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 1, repeatCount: 0)
//        self.sunsetLabel.setText("Updating...")
//    }
    
    func stopAnimations() {
        // sunrise loading image
        self.loadingSunriseImage.stopAnimating()
        self.loadingSunriseImage.setImage(nil)

        // sunset loading image
        self.loadingSunsetImage.stopAnimating()
        self.loadingSunsetImage.setImage(nil)
    }
    
}
